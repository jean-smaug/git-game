# git-game

## Rappels

* `git clone [url_du_dépot]` : clone le projet

## Objectif

L'objectif est de mettre en commun le travail réalisé sur les exercices d'algorithmique d'Exercism.
2 cas possibles :
* Vous êtes le seul a avoir réalisé l'exercice : faites une merge request pour que celle-ci soit validée par un correcteur et d'autres Ing1
* Vous êtes plusieurs a avoir réalisé le même exercice et donc vous devriez avoir des différences d'implémentations (a moins que vous n'essayez de me la faire à l'envers). Ces différences d'implémentation vous permettrons de faire de la résolution de conflits et devront comme précédement être validée par un correcteur via une Merge Request.
